This is a synthetic workflow generator written in Python.
It is based on the implementation from https://github.com/pegasus-isi/WorkflowGenerator

# Structure
The subdirectories are:

    generator       A library for writing synthetic DAX generators
    synthetic       To generate (possible random) synthetic workflows


## generator
Contains everything to generate workflows as dot and dax files.
For details see https://github.com/pegasus-isi/WorkflowGenerator


## Synthetic

### full.py
To generate a workflow from length 10 with width 5
```bash
cd "$PATH/syntehtic"
python fully.py -l 10 -w 4 --dot "../out/fc_10_4.dot" --dax "../out/fc_10_4.dax"
```

This will generate the following workflow:

![Workflow](doc/fc_10_4.png "Workflow with length 10 and width 4")


### straight.py
In contradiction to fully, a task is connected only to one parent.

```bash
cd "$PATH/syntehtic"
python straight.py -l 10 -w 4 --dot "../out/sc_10_4.dot" --dax "../out/sc_10_4.dax"
```

![Workflow](doc/sc_10_4.png "Workflow with length 10 and width 4")


### randlevel.py
For details see https://github.com/pegasus-isi/WorkflowGenerator


### randpair.py
For details see https://github.com/pegasus-isi/WorkflowGenerator
