import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from generator import *
import math
import random

def genstraight(width, numLevels, runtimeDist, sizeDist):       
    workflow = Workflow(name="fully connected workflow", 
        description="""Generated workflow with %d width and %d levels""" % (width, numLevels))
    
    parentLevel = []
    currentLevel = []
    for l in range(numLevels):
        for w in range(width):
            jobId = l * width + w
            outFile = File("task_%d_out.dat" % jobId, size=sizeDist()*GB)
            job = Job(id="task_%d" % jobId, namespace="rand", name="Task", runtime=runtimeDist()*SECONDS, outputs=[outFile])

            # Connect direct predecessor only
            if(len(parentLevel) > 0):
                parent = parentLevel[w]
                for parentOutput in parent.outputs:
                    job.addInput(parentOutput)
            
            currentLevel.append(job)
            workflow.addJob(job)
        
        # Move to next layer, so swap current and parent level
        parentLevel = currentLevel
        currentLevel = []
    
    return workflow


def main(*args):
    class StraightLevel(Main):
        def setoptions(self, parser):
            self.parser.add_option("-w", "--width", dest="width", metavar="n", type="int", default=5,
                help="Number of tasks per level [default: %default]")
            self.parser.add_option("-l", "--levels", dest="levels", metavar="n", type="int", default=10,
                help="Number of levels in the workflow [default: %default]")
            
            self.parser.add_option("", "--rtlow", dest="rtlow", metavar="t", type="float", default=200,
                help="Lower bound on runtime in seconds [default: %default]")
            self.parser.add_option("", "--rthigh", dest="rthigh", metavar="t", type="float", default=200,
                help="Upper bound on runtime in seconds [default: %default]")
            
            self.parser.add_option("", "--slow", dest="slow", metavar="t", type="float", default=2,
                help="Lower bound on file size in GB [default: %default]")
            self.parser.add_option("", "--shigh", dest="shigh", metavar="t", type="float", default=2,
                help="Upper bound on file size in GB [default: %default]")
        
        def genworkflow(self, options):
            return genstraight(options.width, options.levels, 
                UniformDistribution(options.rtlow, options.rthigh), 
                UniformDistribution(options.slow, options.shigh))
    
    StraightLevel().main(*args)

if __name__ == '__main__':
    main(*sys.argv[1:])
